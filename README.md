### KANJI-ROYALE

***

### DESCRIPTION

Kanji-royale is a battle royale kind of game, which focus on helping Japanese language learners study Kanji in a fun and interactive way with other people.

### HOW TO RUN LOCALLY

Import sample database into local server (for ex. using XAMPP).

In folder back-end run command:
`node index.js`

In folder front-end run command:
`ng serve`

### ATRIBUTES

- User accounts system:
    - Registration (in progress),
    - Logging in,
    - Identification using JWT,
- Game progress:
    - Built using WebSocket,
- User statistics (in progress),
- Kanji dictionary (in progress)


### REFERENCES

App is using kanjidic dictionary loaded into MySQL database with:
https://gitlab.com/niejakub02/kanjidicmysqlparser
