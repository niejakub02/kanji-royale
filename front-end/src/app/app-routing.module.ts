import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InterfaceComponent } from './components/interface/interface.component';
import { LobbyComponent } from './components/lobby/lobby.component';
import { SearchComponent } from './components/search/search.component';
import { StartComponent } from './components/start/start.component';
import { guest, isInGame, player, user } from './guards/authentication.guard';

const routes: Routes = [
  //{ path:'', redirectTo: 'start', pathMatch: 'full' },
  { path:'', component: StartComponent, canActivate: [guest] },
  { path:'dashboard', component: InterfaceComponent, children: [
    {
      path: '', component: SearchComponent
    }
  ], canActivate: [user, isInGame] },
  { path:'lobby/:id', component: InterfaceComponent, children: [
    {
      path: '', component: LobbyComponent
    }
  ], canActivate: [player] }, // user
  // { path:'*' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [guest, user, player, isInGame]
})
export class AppRoutingModule { }
