import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ScrollingService } from '../../services/scrolling.service';
import { swipeLogic, TVanimation } from './../../../animations';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.scss'],
  animations: [
    TVanimation
  ]
})
export class DictionaryComponent implements OnInit {

  TV: number = 3;
  PV: number;
  CVObserver: Subscription;
  swipe: string = 'default';
  
  constructor(private scrolling: ScrollingService) { }

  ngOnInit(): void {
    this.CVObserver = this.scrolling.getCV().subscribe(CV => {
      // if (this.TV == CV) {
      //   if (this.PV == this.TV-1) this.animation1 = 'from-top'; // FROM TOP
      //   if (this.PV == this.TV+1) this.animation1 = 'from-bottom'; // FROM BOTTOm
      // }
      this.swipe = swipeLogic(this.TV, this.PV, CV);

      this.PV = CV;
    });
  }

}
