import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { swipeLogic, TVanimation, zoomAnimation, zoomLogic } from '../../../animations';
import { LoginService } from '../../services/login.service';
import { QueueService } from '../../services/queue.service';
import { ScrollingService } from '../../services/scrolling.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [
    TVanimation
  ]
})
export class SearchComponent implements OnInit {
  
  TV: number = 1;
  PV: number;
  CVObserver: Subscription;
  swipe: string = 'default';
  zoom: string = 'default';
  
  @ViewChild('findGameButton')
  button: ElementRef;

  constructor(private queue: QueueService, private login: LoginService, private router: Router, private scrolling: ScrollingService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
  };
  }

  ngOnInit(): void {
    this.CVObserver = this.scrolling.getCV().subscribe(CV => {
      this.swipe = swipeLogic(this.TV, this.PV, CV);
      this.PV = CV;
    });
  }

  connectDisconnect() {
    this.login.checkStatus().toPromise().then(data => {
      if (data.status === 'inGame') {
        console.log("You are already in game 😡");
        this.router.navigate(['lobby/' + data.lobbyID]);
      }

      // !this.queue.isInQueue() = NIE MA SUBSKRYPCJI
      if (data.status === 'idle' || !this.queue.isInQueue()) {
        this.queue.addToQueue();
        this.buttonTitleToggle(true);
      } else if (data.status === 'inQueue') {
        this.queue.deleteFromQueue();
        this.buttonTitleToggle(false);
      }
    });
  }

  buttonTitleToggle(change) {
    return this.button.nativeElement.innerHTML = (change) ? 'Cancel' : 'Find game';
  }
}
