import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { ScrollingService } from '../../services/scrolling.service';
import { swipeLogic, TVanimation } from './../../../animations';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [
    TVanimation
  ]
})
export class ProfileComponent implements OnInit {

  TV: number = 2;
  PV: number;
  CVObserver: Subscription;
  swipe: string = 'default';

  constructor(private scrolling: ScrollingService) { }

  ngOnInit(): void {
    this.CVObserver = this.scrolling.getCV().subscribe(CV => {
      this.swipe = swipeLogic(this.TV, this.PV, CV);
      this.PV = CV;
    });
  }
}
