import {
  Component,
  ElementRef,
  HostListener,
  ViewChild,
  OnInit,
  AfterViewInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';
import { ScrollingService } from '../../services/scrolling.service';

@Component({
  selector: 'app-interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.scss'],
})
export class InterfaceComponent implements OnInit, AfterViewInit {
  title: string = 'kanji-royale';
  scrollingArray: ElementRef[] = [];
  scrollEnabled: boolean = true;

  @ViewChild('start', { read: ElementRef })
  appStart: ElementRef;

  @ViewChild('lobby', { read: ElementRef })
  appLobby: ElementRef;

  @ViewChild('profile', { read: ElementRef })
  appProfile: ElementRef;

  @ViewChild('dictionary', { read: ElementRef })
  appDictionary: ElementRef;

  @ViewChild('navigation')
  navigation: ElementRef;

  @ViewChild('view')
  view: ElementRef;

  CV = 0; // CV = CURRENT VIEW

  constructor(
    private login: LoginService,
    private router: Router,
    private scrolling: ScrollingService
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.scrollingArray.push(this.appStart); // promise
    this.scrollingArray.push(this.appLobby);
    this.scrollingArray.push(this.appProfile);
    this.scrollingArray.push(this.appDictionary);

    let menuOptions = this.navigation.nativeElement.children;
    for (let i = 0; i < menuOptions.length; i++) {
      menuOptions[i].children[0].addEventListener('click', () => {
        if (this.scrollEnabled === true && this.login.isLoggedIn()) {
          this.CV = i;
          this.onScroll(0);
          this.changePageSquare(i);
        }
      });
    }
    // SCROLLOWANIE PASKIEM NAWIGACYJNYM
    this.scrolling.changeView().subscribe((view) => {
      if (this.scrollEnabled) {
        this.CV = view;
        this.changePage();
      }
    });
  }

  @HostListener('window:resize', ['$event']) onWheel(e) {
    this.scrollingArray[this.CV].nativeElement.scrollIntoView();
    // GDY RESIZOWALO TO MOZNA BYLO ZOBACZYC CO JEST WYZEJ LUB PONIZEJ
  }

  // LISTENERY ZEBY SIE NIE SCROLLOWALO JAK ROBI SIE ZOOMA Z CTRL I KOLECZKIEM
  @HostListener('document:keydown', ['$event']) onKeyDown(e) {
    this.scrollEnabled = false;
  }

  @HostListener('document:keyup', ['$event']) onKeyUp(e) {
    this.scrollEnabled = true;
  }

  async onScroll(e) {
    if (this.scrollEnabled && this.login.isLoggedIn()) {
      if (e.deltaY > 0 && this.CV + 1 < this.scrollingArray.length) {
        this.CV++;
        // SCROLLOWANIE DO DOLU I ZABEZPIECZENIE NA DLUGOSC TABLICY ZEBY NIE MOZNA BYLO WYJECHAC
      }
      if (e.deltaY < 0 && this.CV > 0) {
        this.CV--;
        // SCROLLOWANIE DO GORY I ZABEZPIECZENIE NA DLUGOSC TABLICY ZEBY NIE MOZNA BYLO WYJECHAC
      }

      this.changePage();
    }
  }

  scrollAwait() {
    setTimeout(() => {
      this.scrollEnabled = true;
      // ZEBY NIE MOZNA BYLO SCROLLOWAC ZA SZYBKO I EW. CZAS NA ANIMACJE
    }, 800);
  }

  changePageSquare(chosen) {
    let menuOptions = this.navigation.nativeElement.children;

    for (let menuOption of menuOptions) {
      menuOption.children[0].classList.remove('current-page');
      menuOption.children[1].classList.remove('visible');
    }

    menuOptions[chosen].children[0].classList.add('current-page');
    menuOptions[chosen].children[1].classList.add('visible');
  }

  getLoginOn(e) {
    e
      ? this.navigation.nativeElement.classList.add('hide')
      : this.navigation.nativeElement.classList.remove('hide');
  }

  changePage() {
    this.scrolling.setCV(this.CV);
    this.changePageSquare(this.CV);
    this.scrollingArray[this.CV].nativeElement.scrollIntoView();
    this.scrollEnabled = false;
    this.scrollAwait();
  }
}
