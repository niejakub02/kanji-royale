import { CompileStylesheetMetadata } from '@angular/compiler';
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
  HostListener,
  ViewChildren,
  QueryList,
  Renderer2,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { swipeLogic, TVanimation } from '../../../animations';
import { LobbyService } from '../../services/lobby.service';
import { ScrollingService } from '../../services/scrolling.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss'],
  animations: [TVanimation],
})
export class LobbyComponent implements OnInit, AfterViewInit {
  TV: number = 1;
  PV: number;
  CVObserver: Subscription;
  swipe: string = 'default';

  gameStarted = false; // WIEKSZY SCOOPE
  hearts: ElementRef[] = [];
  lobbyID: string;
  lobbyInfoSub: Subscription;
  sbAnswerSub: Subscription;
  sbsOutSub: Subscription;
  youWonSub: Subscription;
  youAreOutSub: Subscription;
  loadHeartsSub: Subscription;
  timer; // : NodeJS.Timeout -> wczesniej dzialalo susge
  avatars: ElementRef[] = [];

  @ViewChild('kanjiCharacter')
  kanjiCharacter: ElementRef;

  @ViewChild('answerInput')
  answerInput: ElementRef;

  @ViewChild('categoryLabel')
  categoryLabel: ElementRef;

  @ViewChild('timerLabel')
  timerLabel: ElementRef;

  @ViewChild('avatarsWrapper')
  avatarsWrapper: ElementRef;

  @ViewChildren('heart')
  heart: QueryList<ElementRef>;

  @HostListener('document:keydown', ['$event']) onEnter(e) {
    // 13 - enter
    if (e.keyCode == 13 && this.gameStarted) this.submitAnswer();
  }

  constructor(
    private route: ActivatedRoute,
    private lobby: LobbyService,
    private renderer: Renderer2,
    private scrolling: ScrollingService
  ) {
    
  }

  ngOnInit(): void {
    this.scrolling.setView(1);
    this.lobbyID = this.route.snapshot.paramMap.get('id');
    this.CVObserver = this.scrolling.getCV().subscribe((CV) => {
      this.swipe = swipeLogic(this.TV, this.PV, CV);
      this.PV = CV;
    });
  }

  async ngAfterViewInit(): Promise<void> {
    this.hearts = [...this.heart.toArray()];
    await this.lobby.establishSocket(this.lobbyID);

    this.gameStarted = await this.lobby.checkLobbyProgress(this.lobbyID);

    if (!this.gameStarted) {
      this.gameStarted = await this.lobby.raportPresence(this.lobbyID);
    }

    console.log(this.gameStarted);

    if (this.gameStarted) {
      console.log('no gra sie juz zxaczela aha');
      // TU SUBSKRYBUJEMY DO WSZYSTKICH POTRZEBNYCH NAM EVENTOW

      // PO OTRZYMIANU PARAMETROW DANEJ RUNDY
      this.lobbyInfoSub = this.lobby
        .onLobbyInfo(this.lobbyID)
        .subscribe((data) => {
          console.log(data);

          data.answered
            ? this.renderer.setProperty(
                this.answerInput.nativeElement,
                'disabled',
                true
              )
            : this.renderer.setProperty(
                this.answerInput.nativeElement,
                'disabled',
                false
              );

          this.answerInput.nativeElement.value = '';

          this.loadPlayers(data.playersNumber);

          clearInterval(this.timer);

          this.timeout(data.roundStartedAt);

          this.categoryLabel.nativeElement.value = data.category;

          this.kanjiCharacter.nativeElement.innerHTML = data.kanji;
        });

      this.loadHeartsSub = this.lobby.onLoadHearts().subscribe((lives) => {
        console.log(lives);
        this.loadHearts(lives)
      });

      // CO ZROBIC JEZELI GRACZ WYGRAL
      this.youWonSub = this.lobby.onYouWon().subscribe(() => {
        console.log('--- WIN ---');
        clearInterval(this.timer);
      });

      // PO CZYJES ODPOWIEDZI, NIE WIEM CZY POTRZEBNE
      // ACZKOLWIEK MOZE SIE PRZYDAC DO JAKIEJS ANIMACJI
      this.sbAnswerSub = this.lobby.onSbAnswer().subscribe((data) => {
        console.log(`${data.index} odpowiedzial ${data.response}`);

        if (data.response) {
          this.renderer.addClass(this.avatarsWrapper.nativeElement.children[data.index], 'player-avatar-correct');
        }
      });

      // PO WYRZCENIU KTOREGOS Z GRACZY, DZIEJE SIE COS Z JEGO AWATAREM
      this.sbsOutSub = this.lobby.onSbsOut().subscribe((data) => {
        this.renderer.addClass(this.avatarsWrapper.nativeElement.children[data], 'player-avatar-out');
        console.log(`${data} is out`);
      });

      // PRZY WYRZUCENIU ZATRZYMAJ TIMER BO JEST NA INTERVALU I DALEJ BY LECIAL
      this.youAreOutSub = this.lobby.onYouAreOut().subscribe(() => {
        console.log('--- OUT ---');
        clearInterval(this.timer);
      });

      // WYSYLAMY ZAPYTANIE O PARAMETRY DANEJ RUNDY
      this.lobby.getLobbyInfo(this.lobbyID);
    }
  }

  async submitAnswer(): Promise<void> {
    let myAnswer = this.answerInput.nativeElement.value;

    if (myAnswer != '') {
      let data = await this.lobby.submitAnswer(this.lobbyID, myAnswer);
      if (!data.answered) {
        // DATA ANSWERED CZYLI CZY JUZ DOBRZE ODPOWIEDZILISMY
        if (data.response) {
          // DOBRA ODPOWIEDZ DAJEMY DISABLED NA ANSWER BUTTON
          // MOZE WYRZUCE TO
          this.renderer.setProperty(this.answerInput.nativeElement, 'disabled', true);
        }
        console.log(`ODPOWIEDZIALES: ${data.response}`);
      } else {
        console.log('You already answered corecctly!');
      }
    }
  }

  timeout(roundStartedAt): void {
    let timeLeft = 20 - Math.round((Date.now() - roundStartedAt) / 1000);

    this.timer = setInterval(() => {
      timeLeft--;
      this.timerLabel.nativeElement.innerHTML = timeLeft;
      if (timeLeft <= 0) clearInterval(this.timer);
    }, 1000);
  }

  loadPlayers(playerNumber): void {
    for (let children of this.avatarsWrapper.nativeElement.children) {
      this.renderer.removeChild(this.avatarsWrapper.nativeElement, children);
    }

    for (let i = 0; i < playerNumber; i++) {
      let random = Math.floor(Math.random() * 5) + 1;
      let avatar = this.renderer.createElement('div');
      this.renderer.addClass(avatar, 'player-avatar');
      this.renderer.setStyle(
        avatar,
        'background-image',
        `url('./assets/avatar-${random}.png`
      );
      this.renderer.appendChild(this.avatarsWrapper.nativeElement, avatar);
      // JAKBY BYL SERWIS OD SCROLLOWANIA TO MOGLBYM TUTAJ ZAZADAC SCROLL INTO VIEW
    }
    // USUNIECIE PLACEHOLDER ZEBY NIE BUGOWAL SIE WIDOK BO JAK TEGO NIE MA TO DODAJE SIE TE AVATARY I
    // PRZEZ TO PRZESUWA SIE WIDOK I WIDAC INNE EKRANY
  }

  loadHearts(lives): void {
    for (let live = lives; live < 3; live++ ) {
      this.hearts[live].nativeElement.style.color = '#000000';
    }
  }
}
