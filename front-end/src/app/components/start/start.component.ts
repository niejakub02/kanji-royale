import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  Input,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { swipeLogic, TVanimation } from '../../../animations';
import { LoginService } from '../../services/login.service';
import { QueueService } from '../../services/queue.service';
import { ScrollingService } from '../../services/scrolling.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
  providers: [],
  animations: [TVanimation],
})
export class StartComponent implements OnInit {
  TV: number = 0;
  PV: number;
  CVObserver: Subscription;
  swipe: string = 'default';
  guest = true;
  user = true;

  @ViewChild('uname')
  uname: ElementRef;

  @ViewChild('pword')
  pword: ElementRef;

  @ViewChild('r_username')
  r_username: ElementRef;

  @ViewChild('r_password')
  r_password: ElementRef;

  @ViewChild('r_repated_password')
  r_repated_password: ElementRef;

  @ViewChild('wrapper')
  wrapper: ElementRef;

  signIn = false;
  signUp = false;

  constructor(
    private login: LoginService,
    private router: Router,
    private queue: QueueService,
    private scrolling: ScrollingService
  ) {}

  ngOnInit(): void {
    this.CVObserver = this.scrolling.getCV().subscribe((CV) => {
      this.swipe = swipeLogic(this.TV, this.PV, CV);
      this.PV = CV;
    });

    if (this.router.url === '/') {
      this.guest = true;
      this.user = false;
    } else {
      this.guest = false;
      this.user = true;
    }
  }

  loginAttempt() {
    let un = this.uname.nativeElement.value;
    let pw = this.pword.nativeElement.value;

    this.login
      .loginAttempt({ username: un, password: pw })
      .toPromise()
      .then((data) => {
        if (data.valid === true) {
          localStorage.setItem('JWT_TOKEN', data.accessToken);
          localStorage.setItem('JWT_REFRESH', data.refreshToken);
          this.router.navigate(['dashboard']);
        }

        if (data.valid === false) {
          console.log('porvalo ale frajer widzowie');
        }
        // BLAD ZE NIEPRAWIDLOWE PASY

        this.signIn = false;
      });
  }

  logout() {
    if (this.login.getAccessToken()) {
      this.login
        .checkStatus()
        .toPromise()
        .then((data) => {
          if (data.status === 'idle') {
            // nic
          }

          if (data.status === 'inQueue') {
            this.queue.deleteFromQueue();
          }

          if (data.status === 'inGame') {
            // jak jest w grze
          }

          this.login
            .logout()
            .toPromise()
            .then((callback) => {
              callback.valid
                ? console.log('Poprawne wylogowanie')
                : console.log('Wystapil blad przy wylogowaniu');
            });
        });
    }
  }

  goNext() {
    if (this.router.url == '/') {
      this.signIn = true;
    }

    if (this.router.url == '/dashboard') {
      this.scrolling.setView(1);
    }
  }

  signUpAttempt() {
    let ru = this.r_username.nativeElement.value;
    let rp = this.r_password.nativeElement.value;
    let rpp = this.r_repated_password.nativeElement.value;

    if (rp == rpp) {
      this.login
        .signUpAttempt({ username: ru, password: rp })
        .toPromise()
        .then((data) => {
          if (data.valid) {
            console.log('pomyślna rejestracja');
          }

          if (!data.valid) {
            console.log('porvalo ale frajer widzowie');
          }

          this.signUp = false;
        });
    } else {
      console.log("hasła się nie zgadzają");
    }
  }
}
