import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartComponent } from './components/start/start.component';
import { SearchComponent } from './components/search/search.component';
import { ProfileComponent } from './components/profile/profile.component';
import { LobbyComponent } from './components/lobby/lobby.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { QueueService } from './services/queue.service';
import { LoginService } from './services/login.service';
import { DictionaryComponent } from './components/dictionary/dictionary.component';
import { SocketService } from './services/socket.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ScrollingService } from './services/scrolling.service';
import { InterfaceComponent } from './components/interface/interface.component';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    SearchComponent,
    ProfileComponent,
    LobbyComponent,
    LoginComponent,
    DictionaryComponent,
    InterfaceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [QueueService, LoginService, SocketService, ScrollingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
