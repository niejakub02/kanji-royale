import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { io, Socket } from 'socket.io-client';
import { AlertService } from './alert.service';
import { LoginService } from './login.service';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root',
})
export class LobbyService {
  private isPlayerInLobby_url = '/api/isPlayerInLobby';
  getLevel: Subscription;
  getKanji: Subscription;
  broadcastAnswer: Subscription;
  sbsOutSub: Subscription;
  onYouWonSub: Subscription;
  onYouAreOutSub: Subscription;
  loadHeartsSub: Subscription;

  constructor(
    private http: HttpClient,
    private login: LoginService,
    private socket: SocketService,
    private router: Router,
    private alert: AlertService
  ) {}

  isPlayerInLobby(lobbyID): Observable<any> {
    console.log(lobbyID);
    return this.http.post(this.isPlayerInLobby_url, { accToken: this.login.getAccessToken(), lobbyID: lobbyID });
  }

  establishSocket(lobbyID): Promise<void> {
    return new Promise(async (resolve, reject) => {
      let socketExists = await this.socket.checkSocket();
      console.log(`socket ${socketExists}`);

      if (!socketExists) {
        await this.socket.connect();
      }

      await this.socket.addSocket(lobbyID, this.login.getAccessToken());

      resolve();
    });
  }

  raportPresence(lobbyID): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      let response = await this.socket.raportPresence(
        this.login.getAccessToken(),
        lobbyID
      );
      resolve(response);
    });
  }

  getLobbyInfo(lobbyID): void {
    this.socket.getLobbyInfo(this.login.getAccessToken(), lobbyID);
  }

  onLobbyInfo(lobbyID): Observable<any> {
    return new Observable((observer) => {
      this.getLevel = this.socket.getLevel().subscribe((data) => {
        observer.next(data);
      });
    });
  }

  submitAnswer(lobbyID, myAnswer): Promise<any> {
    return new Promise(async (resolve, reject) => {
      let data = await this.socket.answer(
        this.login.getAccessToken(),
        lobbyID,
        myAnswer
      );
      resolve(data);
    });
  }

  onSbAnswer(): Observable<any> {
    return new Observable((observer) => {
      this.broadcastAnswer = this.socket.broadcastAnswer().subscribe((data) => {
        observer.next(data);
      });
    });
  }

  checkLobbyProgress(lobbyID): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      let response = await this.socket.checkLobbyProgress(this.login.getAccessToken(), lobbyID);
      this.alert.inGameAlert(response);
      resolve(response);
    });
  }

  onSbsOut(): Observable<any> {
    return new Observable((observer) => {
      this.sbsOutSub = this.socket.onSbsOut().subscribe((data) => {
        observer.next(data);
      });
    });
  }

  onYouWon(): Observable<void> {
    return new Observable((observer) => {
      this.onYouWonSub = this.socket.onYouWon().subscribe(() => {
        this.router.navigate(['dashboard']);
        observer.next();
      });
    });
  }

  onYouAreOut(): Observable<void> {
    return new Observable((observer) => {
      this.onYouAreOutSub = this.socket.onYouAreOut().subscribe(() => {
        this.router.navigate(['dashboard']);
        observer.next();
      });
    });
  }

  onLoadHearts(): Observable<number> {
    return new Observable((observer) => {
      this.loadHeartsSub = this.socket.onLoadHearts().subscribe((lives) => {
        observer.next(lives);
      });
    });
  }
}
