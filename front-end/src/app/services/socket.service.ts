import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { io, Socket } from 'socket.io-client';
import { LoginService } from './login.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  constructor(
    private login: LoginService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  socket: Socket;
  // serverIP = '192.168.1.23';

  // CONNECTION SEGMENT
  connect(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.socket = io(`${environment.ip}:${environment.port}`);

      this.socket.on('connect', () => {
        resolve();
      });
    });
  }

  disconnect(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.socket.disconnect();

      this.socket.on('disconnect', () => {
        resolve();
      });
    });
  }

  // QUEUE SEGMENT
  joinQueue(token): void {
    this.socket.emit('join queue', token);
  }

  leaveQueue(token): void {
    if (!!this.socket) this.socket.emit('delete me from queue', token);
  }

  checkQueue(): Observable<number> {
    // RETURNS QUEUE LENGTH
    return new Observable((observer) => {
      this.socket.on('check queue', (queueLength) => {
        observer.next(queueLength);
      });
    });
  }

  createLobby(): Promise<string> {
    // RETURNS LOBBY ID
    return new Promise((resolve, reject) => {
      this.socket.on('create lobby', (id) => {
        resolve(id);
      });
    });
  }

  // LOBBY SEGMENT
  checkSocket(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // ZAMIANA NA TRUE/FALSE = 2 WYKRZYKNIKI i JESZCZE JEDEN ZEBY BYLO ZE FALSE
      // DWA CASEY - gra zaczyna sie:
      // - odswiezamy strone i dolaczamy do lobby
      // - mamy wlaczona strone na drugiej zakladce i klikamy przycisk find game
      resolve(this.socket ? true : false);
    });
  }

  addSocket(lobbyID, token): Promise<void> {
    return new Promise((resolve, reject) => {
      this.socket.on('adding socket', () => {
        resolve();
      });

      this.socket.emit('add socket', {
        token: token,
        lobbyID: lobbyID,
      });
    });
  }

  raportPresence(token, lobbyID): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.socket.on('game started', () => {
        resolve(true);
      });

      this.socket.emit('presence raport', {
        token: token,
        lobbyID: lobbyID,
      });
    });
  }

  getLevel(): Observable<any> {
    return new Observable((observer) => {
      this.socket.on('current level', (data) => {
        observer.next(data);
      });
    });
  }

  getLobbyInfo(token, lobbyID): void {
    this.socket.emit('lobby info', {
      token: token,
      lobbyID: lobbyID,
    });
  }

  // individual answer
  // broadcast asnwer

  checkLobbyProgress(token, lobbyID): Promise<boolean> {
    return new Promise((resolve, reject) => {
      console.log('socketid:', this.socket.id);
      this.socket.on('game progress', (data) => {
        resolve(data);
      });

      this.socket.emit('check game progress', {
        token: token,
        lobbyID: lobbyID,
      });
    });
  }

  answer(token, lobbyID, myAnswer): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.socket.on('individual answer response', (data) => {
        resolve(data);
      });

      this.socket.emit('check my answer', {
        token: token,
        lobbyID: lobbyID,
        answer: myAnswer,
      });
    });
  }

  broadcastAnswer(): Observable<any> {
    return new Observable((observer) => {
      this.socket.on('broadcast answer response', (data) => {
        observer.next(data);
      });
    });
  }

  onSbsOut(): Observable<any> {
    return new Observable((observer) => {
      this.socket.on('player out', (data) => {
        observer.next(data);
      })
    })
  }

  onYouWon(): Observable<void> {
    return new Observable((observer) => {
      this.socket.on('you won', () => {
        observer.next();
      });
    });
  }

  onYouAreOut(): Observable<any> {
    return new Observable((observer) => {
      this.socket.on('you are out', () => {
        observer.next();
      });
    });
  }

  onLoadHearts(): Observable<number> {
    return new Observable((observer) => {
      this.socket.on('hearts', (lives) => {
        observer.next(lives);
      });
    });
  }
  // getKanji(): Observable<void> {
  //   return new Observable(observer => {
  //     this.socket.on('current kanji', data => {
  //       observer.next(data.kanji);
  //     });
  //   })
  // }
}
