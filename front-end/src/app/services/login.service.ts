import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private login_url = '/api/login';
  private register_url = '/api/signup';
  private refreshToken_url = '/api/refreshToken';
  private logout_url = '/api/logout';
  private checkStatus_url = '/api/checkStatus';
  private changeStatus_url = '/api/changeStatus';

  constructor(private http: HttpClient, private router: Router) {}

  loginAttempt(user): Observable<any> {
    return this.http.post(this.login_url, user);
  }

  signUpAttempt(user): Observable<any> {
    return this.http.post(this.register_url, user);
  }

  isLoggedIn() {
    return !!this.getAccessToken();
  }

  getAccessToken() {
    return localStorage.getItem('JWT_TOKEN');
  }

  getRefreshToken() {
    return localStorage.getItem('JWT_REFRESH');
  }

  logout(): Observable<any> {
    let token = this.getAccessToken();
    if (token) {
      localStorage.removeItem('JWT_TOKEN');
      localStorage.removeItem('JWT_REFRESH');
      this.router.navigate(['']);
      return this.http.post(this.logout_url, { token: token });
    }
  }

  refreshToken(refreshToken): Observable<any> {
    return this.http.post(this.refreshToken_url, {
      refreshToken: refreshToken,
    });
  }

  checkStatus(): Observable<any> {
    return this.http.post(this.checkStatus_url, {
      token: this.getAccessToken(),
    });
  }

  changeStatus(status): Observable<any> {
    return this.http.post(this.changeStatus_url, {
      token: this.getAccessToken(),
      status: status,
    });
  }
}
