import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { LoginService } from './login.service';
import { ScrollingService } from './scrolling.service';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root',
})
export class QueueService {

  checkQueueSub: Subscription;
  createLobbySub: Subscription;

  constructor(
    private login: LoginService,
    private router: Router,
    private route: ActivatedRoute,
    private socket: SocketService,
    private scrolling: ScrollingService
  ) {}

  async addToQueue(): Promise<void> {
    await this.socket.connect();
    this.socket.joinQueue(this.login.getAccessToken());
    this.checkQueueSub = this.socket.checkQueue().subscribe((queueLength) =>
      console.log('Ilosc graczy w kolejce: ' + queueLength)
    );
    this.socket.createLobby().then((id) => { 
      this.router.navigate(['lobby/' + id]);
      this.scrolling.setView(1); // 1 - outlet
      this.checkQueueSub.unsubscribe();
    });
    console.log(this.isInQueue());
  }

  async deleteFromQueue(): Promise<void> {
    console.log(this.isInQueue());

    this.socket.leaveQueue(this.login.getAccessToken());
    this.checkQueueSub.unsubscribe();
    console.log(this.isInQueue());

    await this.socket.disconnect();
  }

  getLobbyID(): string {
    return this.route.snapshot.paramMap.get('id');
  }

  isInQueue(): boolean {
    return this.checkQueueSub ? !this.checkQueueSub.closed : false;
  }
}
