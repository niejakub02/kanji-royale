import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScrollingService {

  CV: number = 0;
  toView: number = 0;

  CVObserver: BehaviorSubject<number> = new BehaviorSubject(this.CV);
  toViewObserver: BehaviorSubject<number> = new BehaviorSubject(this.toView);

  constructor() { }

  setCV(CV):void {
    this.CV = CV;
    this.CVObserver.next(this.CV);
  }

  getCV(): BehaviorSubject<number> {
    return this.CVObserver;
  }
  
  setView(view): void {
    this.toView = view;
    this.toViewObserver.next(this.toView);
  }

  changeView(): BehaviorSubject<number> {
    return this.toViewObserver;
  }
}
