import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  NavigationEnd,
} from '@angular/router';
import { Observable } from 'rxjs';
import { LobbyService } from '../services/lobby.service';
import { LoginService } from '../services/login.service';
/*
@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  
  constructor (private login: LoginService, private router: Router) { }

  canActivate() {
    //console.log(this.login.isLoggedIn());
    //(this.login.isLoggedIn()) ? console.log('eoeo') : console.log('nic');

    console.log(this.login.isLoggedIn());
    if (this.login.isLoggedIn()) {
      this.router.navigate(['dashboard'])
    } else {
      return true;
    }
  }
}*/

@Injectable({
  providedIn: 'root',
})
export class guest implements CanActivate {
  constructor(private login: LoginService, private router: Router) {}

  canActivate() {
    if (this.login.isLoggedIn()) {
      console.log('o ty kurka rurka');
      this.router.navigate(['dashboard']); // XD
      return false;
    }
    return true;
  }
}

@Injectable({
  providedIn: 'root',
})
export class user implements CanActivate {
  constructor(private login: LoginService, private router: Router) {}

  // canActivate() {
  //   if (this.login.isLoggedIn()) {
  //     return true;
  //   }
  //   this.router.navigate(['']); // TEST
  //   return false;
  // }

  canActivate() {
    return this.login
      .checkStatus()
      .toPromise()
      .then((data) => {
        console.log(data);
        if (data) {
          if ((data.status === 'idle') || (data.status === 'inQueue')) {
            return true;
          } 
          else if (data.status === 'offline') {
            localStorage.removeItem('JWT_TOKEN');
            localStorage.removeItem('JWT_REFRESH');
            this.router.navigate(['']);
          }
          else {
            return false;
          }
        } else {
          localStorage.removeItem('JWT_TOKEN');
          localStorage.removeItem('JWT_REFRESH');
          this.router.navigate(['']);
          return false;
        }
      });
    // return this.login.isLoggedIn();
  }
}

@Injectable({
  providedIn: 'root',
})
export class player implements CanActivate {
  constructor(
    private login: LoginService,
    private router: Router,
    private lobby: LobbyService
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    let accessToken = this.login.getAccessToken();
    let lobbyID = route.paramMap.get('id');

    return this.lobby
      .isPlayerInLobby(lobbyID) // getPlayers = get access tokens of players in lobby
      .toPromise()
      .then((data) => {
        if (!!data) {
          // JEZELI LOBBY JESZCZE NIE ISTNIEJE
          if (data) return true;
        }

        this.router.navigate(['dashboard']); // JEZELI LOBBY ISTNIEJE ALE TOKEN GRACZA NIE JEST PRZYPISANY DO LOBBY
        return false;
      });
  }
}

@Injectable({
  providedIn: 'root',
})
export class isInGame implements CanActivate {
  constructor(
    private http: HttpClient,
    private login: LoginService,
    private router: Router
  ) {}

  canActivate() {
    if (!this.login.isLoggedIn()) {
      return false;
    } // jesli nie jestes zalogowany to nawet tego nie sprawdzaj, nwm powinnien guard user dzialac

    return this.login
      .checkStatus()
      .toPromise()
      .then((data) => {
        if (data) {
          if (data.status === 'inGame') {
            this.router.navigate(['lobby/' + data.lobbyID]);
            return false;
          } else {
            return true;
          }
        } else {
          this.router.navigate(['']);
          return false;
        }
      });
  }
}
