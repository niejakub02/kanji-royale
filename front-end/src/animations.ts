import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

export const TVanimation = trigger('TVanimation', [ // thisViewAnimation
    state('from-top', style({
        opacity: 0,
        transform: 'translateY(-50px)',
        filter: 'blur(3px)'
    })),
    state('default', style({
        opacity: 1,
        transform: 'translateY(0px)',
        filter: 'blur(0px)'
    })),
    state('from-bottom', style({
        opacity: 0,
        transform: 'translateY(50px)',
        filter: 'blur(3px)'
    })),
    transition('from-top => default', animate('750ms ease-out')),
    transition('from-bottom => default', animate('750ms ease-out'))
]);

export const zoomAnimation = trigger('zoomAnimation', [ //
    state('zoom', style({
        transform: 'scale(1.05)'
    })),
    state('default', style({
        transform: 'scale(1)',
    })),
    transition('zoom => default', animate('750ms ease-out')),
]);

export const rectanglesAnimation = trigger('rectanglesAnimation', [ //
    state('narrow', style({
        height: '0px',
        filter: 'blur(1px)'
    })),
    state('default', style({
        height: '8px',
        filter: 'blur(0px)'
    })),
    transition('narrow => default', animate('750ms ease-out')),
]);

export const swipeLogic = (TV, PV, CV) => {
    if (TV == CV) {
        if (PV < TV) return 'from-top'; // FROM TOP
        if (PV > TV) return 'from-bottom'; // FROM BOTTOm
      }
}

export const zoomLogic = (TV, CV) => {
    if (TV == CV) return 'zoom';
}

export const rectanglesAnimationLogic = (TV, CV) => {
    if (TV == CV) return 'narrow';
}