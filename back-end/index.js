/// REQUIRES
require('dotenv').config();
const express = require('express');
const socketIo = require('socket.io');
const app = express();

const loginRoutes = require('./routes/loginRoutes.js');
const playerRouter = require('./routes/playerRoutes.js');
const lobbyRouter = require('./routes/lobbyRoutes.js');
const sockets = require('./sockets');


const player = require('./models/playerModel');
const lobbyModel = require('./models/lobbyModel');
const queueController = require('./controllers/queueController');


/// SETTINGS 
app.use(express.json());
app.use(express.static(__dirname + '/static/'));
const server = app.listen(11420, function() {
  // start
});


// DO USUNIĘCIA
// setInterval(() => {
  // console.log('--- PLAYERS ---');
  // console.log(player.getPlayers());
//   console.log('--- QUEUE ---');
  // console.log(queueController.getQueue());
  // console.log('--- LOBBY ---')
  // if(lobbyModel.getLobbies()[0]) {
  //   for (let lobby of lobbyModel.getLobbies()) {
  //     if (!!lobby) console.log('nie-empt');
  //     if (!!!lobby) console.log('empt');
  //   }
  // }
  // console.log(lobby.getLobbies());
// }, 2000);


/// SOCKETS
const io = socketIo(server, {
  cors: {
    origin: `http://${process.env.ip}:${process.env.port}`,
    methods: ["GET", "POST"]
  }
});


io.on("connection", function (socket) {
  console.log(socket.id + " -> new socket");

  socket.on('join queue', (token) => sockets.joinQueue(this, socket, token));
  
  socket.on('delete me from queue', (token) => sockets.leaveQueue(this, socket, token));

  socket.on('check game progress', (data) => sockets.checkLobbyProgress(this, socket, data.token, data.lobbyID));

  socket.on('presence raport', (data) => sockets.presenceRaport(this, socket, data.token, data.lobbyID));

  socket.on('add socket', (data) => sockets.addSocket(socket, data.token, data.lobbyID));

  socket.on('lobby info', (data) => sockets.sendLobbyInfo(this, socket, data.lobbyID, data.token));

  socket.on('check my answer', (data) => sockets.checkAnswer(this, socket, data.token, data.lobbyID, data.answer));
});


/// ROUTING
app.use('', loginRoutes);
app.use('', playerRouter);
app.use('', lobbyRouter);