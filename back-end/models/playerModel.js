class player {
  constructor(id, un, s, lID) {
    this.ID = id;
    this.username = un;
    this.status = s;
    this.lobbyID = lID;
  }

  changeStatusX(s) {
    this.status = s;
  }

  changeLobbyID(lID) {
    this.lobbyID = lID;
  }

  static add(id, un, s, lID) {
    return players.push(new player(id, un, s, lID));
  }

  static getPlayers() {
    return players;
  }

  static delete(id) {
    let index = players.findIndex((player) => player.ID == id);
    return players.splice(index, 1);
  }
}

const players = [];

module.exports = player;
