class lobby {
  constructor(lID, p) {
    this.lobbyID = lID;
    this.players = [...p];
    this.inProgress = false;
    this.level = 0;
    this.literal = "";
    this.category = "";
    this.answers = [];
    this.roundStartedAt = 0;
    this.timeOut = false;
  }

  getPlayersIDs() {
    let IDs = [];

    for (let player of this.players) {
      IDs.push(player.ID);
    }

    return IDs;
  }

  addSocket(socket) {
    return this.players.sockets.push(socket);
  }

  toggleProgress() {
    this.inProgress = !this.inProgress;
  }

  getLevel() {
    return this.level;
  }

  getCategory() {
    return this.category;
  }

  getLiteral() {
    return this.literal;
  }

  getAnswers() {
    return this.answers;
  }

  getLobbyProgress() {
    return this.inProgress;
  }

  getRoundStart() {
    return this.roundStartedAt;
  }

  getPlayersNumber() {
    return this.players.length;
  }

  setLobbyProgress(bool) {
    this.inProgress = bool;
  }

  setRoundStart() {
    this.roundStartedAt = Date.now();
  }

  resetAnswers() {
    for (let player of this.players) {
      player.answer = false;
    }
  }

  setTimeout() {
    let level = this.level;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (level == this.level) resolve();
        // TRZEBA JAKOS ZHANDLOWAC REJECT
        // (level == this.level) ? resolve() : reject();
      }, process.env.ROUND_TIME);
    });
  }

  setupLobby(literal, category, answers) {
    this.level++;
    this.literal = literal;
    this.category = category;
    this.answers = [...answers];
  }

  toggleAnswer(index) {
    this.players[index].answer = !this.players[index].answer;
  }

  deletePlayerFromList(index) {
    return this.players.splice(index, 1);
  }

  nextLevelKa() {
    // let answers = 0;
    // for (let player of this.players) {
    //   if (player.anserwed) answers++;
    // }

    // return answers == process.env.MAX_PLAYERS - this.level ? true : false;
    return true;
  }

  static create(lID, p) {
    return lobbies.push(new lobby(lID, p));
  }

  static delete(lobbyID) {
    let index = lobbies.findIndex((lobby) => (!!lobby && lobby.lobbyID == lobbyID));
    delete lobbies[index];
  }

  static getLobbies() {
    return lobbies;
  }

  //   static getLobbiesWIDs() {
  //     let maps = [];

  //     for (let lobby of lobbies) {
  //       for (let player of lobby.players) {
  //         let map = player.sockets.map((socket) => socket.id);
  //         maps.push(map);
  //       }

  //       console.log(maps);
  //     }
  //   }
}

const lobbies = [];

module.exports = lobby;
