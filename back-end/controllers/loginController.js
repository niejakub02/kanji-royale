const mysql = require("mysql");
const player = require("./../models/playerModel");
const playerController = require("./playerController");
const token = require("./tokenController");

const loginAttempt = (req, res) => {
  const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "kanji-royale",
  });

  connection.connect(function (err) {
    if (err) throw err;
  });

  let query =
    "SELECT `id`,`login` FROM `users` WHERE `login` LIKE '" +
    req.body.username +
    "' AND `password` LIKE '" +
    req.body.password +
    "'";

  connection.query(query, function (err, results) {
    if (!!results[0]) {
      // CHECKS IF LOGGED IN
      let players = player.getPlayers();
      for (let player of players) {
        if (player.ID == results[0].id) {
          console.log("uzytkownik jest juz zalogowany");
          res.json({ valid: false });
          return res.end();
        }
      }

      let accessToken = token.generateAccessToken(results[0]);
      let refreshToken = token.generateRefreshToken(results[0]);
      // refreshTokens.push(refreshToken);

      player.add(results[0].id, results[0].login, "idle", "");

      connection.end();
      res.json({
        valid: true,
        accessToken: accessToken,
        refreshToken: refreshToken,
      });
    } else {
      res.json({ valid: false });
    }
  });
};

const logoutAttempt = (req, res) => { // TO DO: case kiedy jest w grze/ cos w lobby usunac i sockety
  let payload = token.getTokenPayload(req.body.token);
  player.delete(payload.user.id);
  return res.json({ valid: true }); // return res.end();
};

const signupAttempt = (req, res) => {
  const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "kanji-royale",
  });

  connection.connect(function (err) {
    if (err) throw err;
  });

  let query = `INSERT INTO users (login, password) VALUES ('${req.body.username}', '${req.body.password}')`;

  connection.query(query, function (err, results) {
    if (err) {
      res.json({ valid: false }) 
    }  else {
      res.json({ valid: true })
    }
  });
}

module.exports = {
  loginAttempt,
  logoutAttempt,
  signupAttempt
};
