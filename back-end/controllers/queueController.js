const token = require('./tokenController');

const queue = [];

const addToQueue = (accToken, socket) => {
  let playerInQueue = false;
  let ID = token.getPlayerID(accToken);

  for (let player of queue) {
    if (player.ID === ID) {
      player.sockets.push(socket);
      playerInQueue = true;
    }
  }

  if (!playerInQueue) {
    queue.push({ accToken: accToken, ID: ID, sockets: [socket] });
  }
};

const deleteFromQueue = (accToken) => {
  let index = findInQueue(accToken);
  return queue.splice(index, 1);
};

const findInQueue = (accToken) => {
  let ID = token.getPlayerID(accToken);
  let index = queue.findIndex((player) => player.ID == ID);
  return index;
}

const getQueueLength = () => {
  return queue.length;
};

const getQueue = () => {
  return queue;
};

const shiftQueue = () => {
  return queue.shift();
};

module.exports = {
  addToQueue,
  getQueueLength,
  getQueue,
  shiftQueue,
  deleteFromQueue
};
