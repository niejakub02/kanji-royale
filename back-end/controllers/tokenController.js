const jwt = require("jsonwebtoken");

const authenticateToken = (req, res, next) => {
  let authHeader = req.headers["authorization"];
  let accToken = authHeader.split(" ")[1];

  if (accToken == null) return res.sendStatus(401);

  jwt.verify(accToken, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    req.user = user;
    next();
  });
};

const generateAccessToken = (user) => {
  return jwt.sign({ user: user }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: "3h",
  });
};

const generateRefreshToken = (user) => {
  return jwt.sign({ user: user }, process.env.REFRESH_TOKEN_SECRET);
};

const getTokenPayload = (accToken) => {
  // if (!!accToken) return { 'valid': false }; // cos zrobic jak zly token jaki
  let payload = accToken.split(".")[1];
  let payloadStr = Buffer.from(payload, "base64").toString();
  return JSON.parse(payloadStr);
};

const getPlayerID = (accToken) => {
  return getTokenPayload(accToken).user.id;
}

module.exports = {
  authenticateToken,
  generateAccessToken,
  generateRefreshToken,
  getTokenPayload,
  getPlayerID
};
