const token = require("./tokenController");
const player = require("./../models/playerModel");

const checkStatusRoute = (req, res) => {
  let playerStatus = getPlayerStatus(req.body.token);
  res.json({ status: playerStatus.status, lobbyID: playerStatus.lobbyID });
};

const changeStatusRoute = (req, res) => {
  return changeStatus(req.body.token, req.body.status); //, req.body.lobbyID)
}

const findPlayer = (accToken) => {
  let ID = token.getPlayerID(accToken);
  let players = player.getPlayers();

  for (let player of players) {
    if (player.ID === ID) return player;
  }

  // return user nie istnieje
};

const getPlayerStatus = (accToken) => {
  let player = (!!accToken) ? findPlayer(accToken) : undefined;

  return !!player
    ? { status: player.status, lobbyID: player.lobbyID }
    : { status: "offline" };
};

const changeStatus = (accToken, status, lobbyID) => {
  let player = findPlayer(accToken);
  
  //if (!!lobbyID) userStatus.lobbyID = lobbyID;
  if (status === 'inGame') player.changeLobbyID(lobbyID);

  return !!player ? player.changeStatusX(status) : { status: "offline" };
};

const getOutOfLobby = (accToken) => {
  let player = findPlayer(accToken);

  if (!!player) {
    player.changeStatusX('idle');
    player.changeLobbyID('');
  }
};

module.exports = {
  checkStatusRoute,
  changeStatusRoute,
  findPlayer,
  getPlayerStatus,
  changeStatus,
  getOutOfLobby
};
