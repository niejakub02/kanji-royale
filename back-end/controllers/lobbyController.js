const lobby = require("./../models/lobbyModel");
const crypto = require("crypto");
const queueController = require("./queueController");
const playerController = require("./playerController");
const token = require("./tokenController");

const mysql = require("mysql");

const createLobby = () => {
  let id = crypto.randomBytes(20).toString("hex");
  let playersInLobby = [];

  for (let i = 0; i < process.env.MAX_PLAYERS; i++) {
    let playerInfo = queueController.shiftQueue(); // USUWA PIERWSZEGO GRACZA Z KOLEJKI I GO ZWRACA

    playersInLobby.push({
      token: playerInfo.accToken,
      ID: playerInfo.ID,
      sockets: playerInfo.sockets,
      lives: 3,
      answer: false,
      isInLobby: false,
    });

    playerController.changeStatus(playerInfo.accToken, "inGame", id);
  }

  lobby.create(id, playersInLobby);

  return { playersInLobby: playersInLobby, lobbyID: id };
};

const findLobby = (lobbyID) => {
  const lobbies = lobby.getLobbies();

  for (let lobby of lobbies) {
    if (!!lobby && (lobby.lobbyID === lobbyID)) {
      return lobby;
    }
  }
};

const isPlayerInLobby = (req, res) => {
  console.log(req.body.lobbyID);
  let lobby = findLobby(req.body.lobbyID);
  let IDs = lobby.getPlayersIDs();
  let ID = token.getPlayerID(req.body.accToken);

  IDs.includes(ID) ? res.json({ valid: true }) : res.json({ valid: false });
};

const setupLobby = (lobby) => {
  return new Promise((resolve, reject) => {
    lobby.resetAnswers();

    const connection = mysql.createConnection({
      host: "localhost",
      user: "root",
      password: "",
      database: "kanji-royale",
    });

    connection.connect(function (err) {
      if (err) throw err;
    });


    let category = randCategory();
    
    // SELECT `kanji`, CONCAT( `Reading within Joyo`, ", ",`Reading beyond Joyo` ) AS `readings` FROM `kanji-data` WHERE `id`=4
    // let randomID = Math.floor(Math.random() * (2136 - 1 + 1)) + 1;
    // let query = "SELECT literal, " + category + " as 'answers' FROM `kanji` WHERE `grade` AND " + category + "!='' in (1,2,3,4,5) ORDER BY RAND() LIMIT 1";

    let query = `SELECT literal, ${category} AS 'answers' FROM kanji WHERE grade IN (1,2,3,4,5) AND ${category} != '' ORDER BY RAND() LIMIT 1`;
    
    connection.query(query, async function (err, results) {
      if (!!results[0]) {
        await lobby.setupLobby(
          results[0].literal,
          category,
          results[0].answers.split(", ")
        );
      }

      connection.end();

      lobby.setRoundStart();
      resolve();
    });
  });
};

const randCategory = () => {
  let random = Math.floor(Math.random() * 3);

  switch(random) {
    case 0:
      return 'meanings';
    break;

    case 1:
      return 'on_readings';
    break;
    
    case 2:
      return 'kun_readings';
    break;
  }
};

module.exports = {
  createLobby,
  findLobby,
  setupLobby,
  isPlayerInLobby
};
