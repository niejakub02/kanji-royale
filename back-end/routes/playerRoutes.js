const express = require("express");
const playerController = require("./../controllers/playerController");

const playerRouter = new express.Router();


playerRouter.post("/api/checkStatus", playerController.checkStatusRoute);
playerRouter.post("/api/changeStatus", playerController.changeStatusRoute);

module.exports = playerRouter;
