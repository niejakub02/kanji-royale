const express = require("express");
const loginController = require("../controllers/loginController");

const loginRouter = new express.Router();

loginRouter.post("/api/login", loginController.loginAttempt);
loginRouter.post("/api/logout", loginController.logoutAttempt);
loginRouter.post("/api/signup", loginController.signupAttempt);

module.exports = loginRouter;
