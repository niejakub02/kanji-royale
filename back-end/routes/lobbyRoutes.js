const express = require("express");
const lobbyController = require("../controllers/lobbyController");

const lobbyRouter = new express.Router();

lobbyRouter.post("/api/isPlayerInLobby", lobbyController.isPlayerInLobby);

module.exports = lobbyRouter;
