const button = document.querySelector('input');

const socket = io.connect();

button.addEventListener('click', function() {
    socket.emit('join queue');
});

socket.on('create lobby', (id) => {
    console.log(socket.id);
    socket.emit('clear me from queue');
    window.location.href = 'http://127.0.0.1:11420/lobby/' + id;
});

socket.on('check queue', (number) => {
    console.log(number + ' w kolejce');
});

/*
socket.on('queue', function(data) {
    console.log(data);
    if (data == 2) {
        socket.emit('clear me from queue', socket.id);
        window.location.href = "http://www.youtube.com";
    }
});*/
