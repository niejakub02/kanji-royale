const playerController = require("./controllers/playerController");
const lobbyController = require("./controllers/lobbyController");
const queueController = require("./controllers/queueController");
const tokenController = require("./controllers/tokenController");
const lobbyModel = require("./models/lobbyModel");

const joinQueue = (io, socket, token) => {
  socket.join("queue");

  queueController.addToQueue(token, socket);
  playerController.changeStatus(token, "inQueue");

  let queueLength = queueController.getQueueLength();
  io.to("queue").emit("check queue", queueLength);

  if (queueLength >= process.env.MAX_PLAYERS) {
    let lobbyInfo = lobbyController.createLobby();

    for (let player of lobbyInfo.playersInLobby) {
      for (let sock of player.sockets) {
        sock.join(lobbyInfo.lobbyID);
        sock.join(player.ID);
        sock.leave("queue");
      }
    }

    io.to(lobbyInfo.lobbyID).emit("create lobby", lobbyInfo.lobbyID);
  }
};

const leaveQueue = (io, socket, token) => {
  queueController.deleteFromQueue(token);
  playerController.changeStatus(token, "idle");
  let queueLength = queueController.getQueueLength();

  io.to("queue").emit("check queue", queueLength);
};

const checkLobbyProgress = (io, socket, token, lobbyID) => {
  let lobby = lobbyController.findLobby(lobbyID);
  let ID = tokenController.getPlayerID(token);

  io.to(ID).emit("game progress", lobby.getLobbyProgress());
};

const presenceRaport = async (io, socket, token, lobbyID) => {
  // CHECK IF ALL PLAYERS ARE IN
  let lobby = lobbyController.findLobby(lobbyID);
  let ID = tokenController.getPlayerID(token);

  for (let player of lobby.players) {
    if (ID === player.ID) player.isInLobby = true;
  }

  let raports = 0;

  for (let player of lobby.players) {
    if (player.isInLobby) raports++;
  }

  if (raports == process.env.MAX_PLAYERS) lobby.setLobbyProgress(true);

  if (lobby.getLobbyProgress()) {
    //SETUP LOBBY
    await lobbyController.setupLobby(lobby);
    io.to(lobbyID).emit("game started");
    lobby.setTimeout().then(() => {
      checkLobby(io, lobby);
    });
  }
};

const addSocket = (socket, token, lobbyID) => {
  // io tez chyba zbedne bo tu tylko konkretny socket dostaje info o dodaniu
  // console.log(`socket: ${socket.id}`);
  // console.log(`token: ${token}`);
  // console.log(`lobbyID: ${lobbyID}`);

  let lobby = lobbyController.findLobby(lobbyID);
  let ID = tokenController.getPlayerID(token);

  for (let player of lobby.players) {
    if (ID === player.ID) player.sockets.push(socket);
  }

  socket.join(lobbyID);
  socket.join(ID);
  socket.emit("adding socket");
};

const sendLobbyInfo = (io, socket, lobbyID, token) => {
  // io tez chyba zbedne bo tu tylko konkretny socket dostaje info, token to samo
  let lobby = lobbyController.findLobby(lobbyID);
  let ID = tokenController.getPlayerID(token);
  let index = lobby.players.findIndex((player) => player.ID == ID);

  socket.emit("current level", {
    level: lobby.getLevel(),
    category: lobby.getCategory(),
    kanji: lobby.getLiteral(),
    roundStartedAt: lobby.getRoundStart(),
    playersNumber: lobby.getPlayersNumber(),
    answered: lobby.players[index].answer,
  });

  socket.emit("hearts", lobby.players[index].lives);
};

const checkAnswer = (io, socket, token, lobbyID, answer) => {
  let lobby = lobbyController.findLobby(lobbyID);
  let ID = tokenController.getPlayerID(token);

  let response = lobby.getAnswers().includes(answer);
  let index = lobby.players.findIndex((player) => player.ID == ID);

  if (lobby.players[index].answer) {
    io.to(ID).emit("individual answer response", {
      answered: lobby.players[index].answer
    });
    return;
  }
  // jak zle odpowiedzial to dawac zycko, jak dobrze to mu zaznacz ze dobrze odpwoeidzial
  // response ? lobby.toggleAnswer(index) : lobby.players[index].lives--;
  if (!response) lobby.players[index].lives--;

  io.to(ID).emit("individual answer response", {
    response: response,
    answered: lobby.players[index].answer
  });

  io.to(ID).emit("hearts", lobby.players[index].lives);

  // PO EMICIE ZEBY WYEMITOWAC Z ANSWERED = FALSE, ZEBY ZDISABLOWALO ANSWER INPUT
  if (response) lobby.toggleAnswer(index);

  // KICK PLAYER NIE USUWA Z LISTY Z INSTANCJI Z LOBBY // STARE
  // MUSZE USUWAC OSOBNO Z LISTY Z LOBBY, BO W PRZYPADKU JAK MINIE TIMER TO
  // USUWAM CALA LISTE INDEXOW, A TU MUSZE TYLKO JEDEN
  if (lobby.players[index].lives == 0) {
    kickPlayer(io, lobby, token);
    lobby.deletePlayerFromList(index);
  }

  io.to(lobbyID).emit("broadcast answer response", {
    index: index,
    response: response,
  });

  // CASE KIEDY WSZYSCY ODPOWIEDZIELI WIEC PO CO CZEKAC NA KONIEC RUNDY CZASOWY @@@@@@@@@@@@@@@
  allPlayersAnsweredKa(io, lobby);
};

const nextLevel = async (io, lobby) => {
  if (lobby.players.length == 1) {
    let ID = lobby.players[0].ID;
    let token = lobby.players[0].token;
    let index = lobby.players.findIndex((player) => player.ID == ID);

    io.to(ID).emit("you won");
    socketsDisconnect(lobby, index);
    lobby.deletePlayerFromList(index);
    playerController.getOutOfLobby(token);
    // gracz wygral lobby jest puste
    lobbyModel.delete(lobby.lobbyID);
  } else if (!!lobby && lobby.players.length == 0) {
    // TU JAK I W LOBBYMODEL ORAZ LOBBYCONTROLLER JEST !!LOBBY BO ITEMY W TABLICY
    // MOGA BYC EMPTY CZYLI COS JAK UNDEFINED I TO NAM SKIPUJE TE ELEMENTY
    // wszyscy gracze odpadli
    lobbyModel.delete(lobby.lobbyID);
  } else {
    await lobbyController.setupLobby(lobby);
    io.to(lobby.lobbyID).emit("current level", {
      level: lobby.getLevel(),
      category: lobby.getCategory(),
      kanji: lobby.getLiteral(),
      roundStartedAt: lobby.getRoundStart(),
      playersNumber: lobby.getPlayersNumber(),
      answered: false
    });

    for (let player of lobby.players) {
      io.to(player.ID).emit('hearts', player.lives);
    }

    lobby.setTimeout().then(() => {
      checkLobby(io, lobby);
    });
  }
};

const socketsDisconnect = (lobby, index) => {
  for (let socket of lobby.players[index].sockets) {
    socket.disconnect();
  }

  lobby.players[index].sockets = [];
};

const allPlayersAnsweredKa = (io, lobby) => {
  let playersAnswers = 0;

  for (let player of lobby.players) {
    if (player.answer) playersAnswers++;
  }

  if (playersAnswers == lobby.players.length) {
    // ANULOWAC PROMISA JAKOS
    nextLevel(io, lobby);
  }
};

const checkLobby = (io, lobby) => {
  // usuwamy graczy ktorzy nie odpowiedzieli jeszcze
  let indexes = [];

  for (let player of lobby.players) {
    if (!player.answer) indexes.push(kickPlayer(io, lobby, player.token));
  }

  for (let index of indexes) {
    // usuniecie graczy z listy z instancji lobby dopiero gdy przeiterujemy po tablicy
    // bo jak usuwamy w kickPlayer to tablica sie przesuwa
    lobby.deletePlayerFromList(index);
  }

  console.log(lobby.players);

  nextLevel(io, lobby);
};

const kickPlayer = (io, lobby, token) => {
  let ID = tokenController.getPlayerID(token);
  let index = lobby.players.findIndex((player) => player.ID == ID);

  io.to(lobby.lobbyID).emit("player out", index); // po stronie serwera trzeba sie pozbyc gracza i jakis event moze uruchomic zeby sprawdzic ich ilosc
  io.to(ID).emit("you are out");
  socketsDisconnect(lobby, index);
  // usuniecie z listy graczy w lobby - WAZNE BO PROMISE OD LOBBY SIE WYKONUJE I MOZE BUGOWAC SIE W INNYCH LOBBY
  // WIEC NIE MOZE WYSYLAC DO TEGO GRACZA BO BEDZXIE NA LISCIE LOBBY
  // lobby.deletePlayerFromList(index);
  playerController.getOutOfLobby(token);
  return index;
};

const sendHeartsStatus = (io, lobby, token) => {
  let ID = tokenController.getPlayerID(token);
  let index = lobby.players.findIndex((player) => player.ID == ID);

  io.to(ID).emit('hearts', {
    lives: lobby.players[index].lives
  });
}

module.exports = {
  joinQueue,
  leaveQueue,
  checkLobbyProgress,
  presenceRaport,
  addSocket,
  sendLobbyInfo,
  checkAnswer,
};
